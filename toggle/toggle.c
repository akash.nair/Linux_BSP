/**************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file toggle.c
 * @brief (This code is used to control status LED of beaglebone board while we insert and remove 
 *			device driver in beaglebone board.) 
 *
 * @Author     	- 	Akash Nair
 * @bug    		- 	no known bugs
 **************************************************************************************************
 * History
 *     
 * June/06/2018, Akash Nair , Created (description)
**************************************************************************************************/

#include "toggle.h"

static int __init status_led_start(void)
{
	int loop;
	printk(KERN_INFO "Initializing the GPIO_TEST LKM\n");
	// Is the GPIO a valid GPIO number (e.g., the BBG has 4x32 but not all available)
	if (!gpio_is_valid(gpioLED)){
	  printk(KERN_INFO "Invalid LED GPIO\n");
	  return -ENODEV;
	}
	// Going to set up the LED. It is a GPIO in output mode and will be on by default
	ledOn = true;
	gpio_request(gpioLED, "sysfs");          // gpioLED is hardcoded to 56, request it
	gpio_direction_output(gpioLED, ledOn); // Set the gpio to be in output mode and on
	mdelay(3000);				//Note : When connected to USB make delay (1500) for 3 secs
	gpio_export(gpioLED, false);         // Causes gpio56 to appear in /sys/class/gpio
	gpio_set_value(gpioLED,false);
	mdelay(3000);
	// the bool argument prevents the direction from being changed
	for(loop = 0; loop < 10; loop++) {
	  gpio_set_value(gpioLED,true);
	  mdelay(100);
	  gpio_set_value(gpioLED,false);
	  mdelay(3000);
	}
	return 0;
}

static void __exit status_led_end(void)
{
	printk(KERN_INFO "Goodbye...! Exited from Kernel\n");
	gpio_set_value(gpioLED, false);
	return;
}

module_init(status_led_start);		//executes when insmod used
module_exit(status_led_end);		//executes when rmmod used

MODULE_LICENSE("GPL");		//defines GPL(General Public Licence)
