/**************************************************************************************************
 * @file toggle.h
 * @brief (Function prototype for start and exit from driver modules.)
 * 
 * These contains prototypes for setbits function and eventually
 * any macros, constsants, or global variables you need.
 * 
 * @author Akash Nair
 * @bug No known bugs.
 *************************************************************************************************/

/*****************
 *Includes
 **************/
#include <linux/module.h>     /* Needed by all modules */                                           
#include <linux/kernel.h>     /* Needed for KERN_INFO */                                            
#include <linux/init.h>       /* Needed for the macros */                                           
#include<linux/gpio.h>        /* Needed for the access gpio */                                      
#include<linux/delay.h>       /* Needed for the delay */ 

/**************************
 *Defines
 **********************/
/*NONE*/

/***********************
 *Global variables
 ************************/
 static bool ledOn = 0;                                                                              
 static unsigned int gpioLED = 56; 

/**********************
 *Function prototype
 ************************/
/*************************************************************************************************
 *@brief (This function is used by module_init function and contains code for status led control
 * 		  of beaglebone board.)
 * 
 *@param void (this function takes nothing as arguments parameters.)
 *          
 *@return int (it returns int value)
 *********************************************************************/
static int __init status_led_start(void);


/*************************************************************************************************
 *@brief (This function is used by module_exit function and contains code for status led control
 * 		  of beaglebone board.)
 * 
 *@param void (this function takes nothing as arguments parameters.)
 *          
 *@return int (it returns nothing)
 *********************************************************************/
static void __exit status_led_end(void);
