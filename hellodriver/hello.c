/************************************************************************************************** 
 * Copyright(c) <2018>, Volansys Technologies                                                       
 *                                                                                                  
 * Description:                                                                                     
 * @file hello.c                                                                                   
 * @brief (This code is used to print or information message in kernel log while we insert and   
 *          remove device driver in beaglebone board in run time.)                                                     
 *                                                                                                  
 * @Author      -   Akash Nair                                                                   
 * @bug         -   no known bugs                                                                   
 ************************************************************************************************** 
 * History                                                                                          
 *                                                                                                  
 * June/05/2018, Akash , Created (description)                                                     
**************************************************************************************************/ 
                                                                                                    
#include "hello.h" 

static int __init hello_module_init(void)
{
	printk (KERN_INFO "hello test app module init\n");
	return 0;
}

static void __exit hello_module_cleanup(void)
{
	printk(KERN_INFO "hello test app module cleanup\n");
	return;
}

module_init(hello_module_init);			//executes when insmod used
module_exit(hello_module_cleanup);		//executes when rmmod used 

MODULE_LICENSE("GPL");			//defines GPL(General Public Licence) 
