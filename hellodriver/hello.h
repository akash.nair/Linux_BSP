/************************************************************************************************** 
 * @file hello.h                                                                                    
 * @brief (Function prototype for start and exit from driver modules.)                               
 *                                                                                                  
 * These contains prototypes for setbits function and eventually                                    
 * any macros, constsants, or global variables you need.                                            
 *                                                                                                  
 * @author Akash Nair                                                                            
 * @bug No known bugs.                                                                              
 *************************************************************************************************/ 
                                                                                                    
/*****************                                                                                  
 *Includes                                                                                          
 **************/                                                                                    
#include <linux/module.h>     /* Needed by all modules */                                           
#include <linux/kernel.h>     /* Needed for KERN_INFO */                                            
#include <linux/init.h>       /* Needed for the macros */                                           
                                                                                                    
/**************************                                                                         
 *Defines                                                                                           
 **********************/                                                                            
/*NONE*/                                                                                            
                                                                                                    
/***********************                                                                            
 *Global variables                                                                                  
 ************************/                                                                          
/*NONE*/                                                                                            
                                                                                                    
/**********************                                                                             
 *Function prototype                                                                                
 ************************/                                                                          
/*************************************************************************************************  
 *@brief (This function is used by module_init function and contains code for print message in
 *        kernel log while it executed.)                                   
 *                                                                                                  
 *@param void (this function takes nothing as arguments parameters.)                                
 *                                                                                                  
 *@return int (it returns int value)                                                                
 *********************************************************************/                             
static int __init hello_module_init(void);

/*************************************************************************************************  
 *@brief (This function is used by module_exit function and contains code for print message in   
 *        kernel log while it executed.)                                                                     
 *                                                                                                  
 *@param void (this function takes nothing as arguments parameters.)                                
 *                                                                                                  
 *@return int (it returns nothing)                                                                  
 *********************************************************************/                             
static void __exit hello_module_cleanup(void);
